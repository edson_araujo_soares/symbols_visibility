#include <string>
#include <stdexcept>
#include "gtest/gtest.h"
#include "Foundation/Singleton.h"
#include "IdentityAccess/SessionService.h"


TEST(SessionServiceTest, invalidate)
{

    auto singleton = Foundation::Singleton::instance();
    singleton->createInternalObject();
    singleton->send("Hello!!");
    ASSERT_EQ("Hello!!", singleton->receive());

    IdentityAccess::SessionService sessionService;
    sessionService.invalidate();
    ASSERT_THROW(singleton->receive(), std::logic_error);

}
