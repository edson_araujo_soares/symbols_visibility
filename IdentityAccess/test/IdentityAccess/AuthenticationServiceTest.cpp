#include <string>
#include "gtest/gtest.h"
#include "Foundation/Singleton.h"
#include "IdentityAccess/AutenticationService.h"


TEST(AuthenticationServiceTest, authenticate)
{

    IdentityAccess::AuthenticationService authenticationService;
    authenticationService.authenticate();

    auto singleton = Foundation::Singleton::instance();
    singleton->send("Hello!!");
    ASSERT_EQ("Hello!!", singleton->receive());

}
