#include "Foundation/Singleton.h"
#include "Adapter/Something/HiddenClass.h"
#include "IdentityAccess/AutenticationService.h"

namespace IdentityAccess {


    void AuthenticationService::authenticate()
    {
        // This class is not invokable from here (Internal Symbol)
        Something::HiddenClass hiddenClass;
        // hiddenClass.hiddenAction();

        // This must be invokable but only one instance (Protected Symbol)
        auto singleton = Foundation::Singleton::instance();
        singleton->createInternalObject();
    }


}
