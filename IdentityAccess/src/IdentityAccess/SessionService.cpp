#include "Foundation/Singleton.h"
#include "Adapter/Something/HiddenClass.h"
#include "IdentityAccess/SessionService.h"

namespace IdentityAccess {


    void SessionService::invalidate()
    {
        // This class is not invokable from here (Internal Symbol)
        Something::HiddenClass hiddenClass;
        // hiddenClass.hiddenAction();

        // This must be invokable but only one instance (Protected Symbol)
        auto singleton = Foundation::Singleton::instance();
        singleton->destroyInternalObject();
    }


}
