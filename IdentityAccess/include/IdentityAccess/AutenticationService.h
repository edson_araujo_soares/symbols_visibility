#ifndef IdentityAccess_AuthenticationService_INCLUDED
#define IdentityAccess_AuthenticationService_INCLUDED

#include "Foundation/SymbolVisibilityManager.h"

namespace IdentityAccess {


    class API AuthenticationService
    {
    public:
        AuthenticationService() = default;
        void authenticate();

    };


}

#endif
