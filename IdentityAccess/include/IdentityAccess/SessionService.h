#ifndef IdentityAccess_SessionService_INCLUDED
#define IdentityAccess_SessionService_INCLUDED

#include "Foundation/SymbolVisibilityManager.h"

namespace IdentityAccess {


    class API SessionService
    {
    public:
        SessionService() = default;
        void invalidate();

    };


}

#endif
