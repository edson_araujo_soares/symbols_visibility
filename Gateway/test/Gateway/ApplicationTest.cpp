#include <string>
#include <iostream>
#include <Foundation/Singleton.h>
#include "gtest/gtest.h"
#include "Poll/PollService.h"
#include "IdentityAccess/SessionService.h"
#include "IdentityAccess/AutenticationService.h"

/*
TEST(ApplicationTest, OrchestrationTest)
{

    IdentityAccess::AuthenticationService authenticationService;
    ASSERT_NO_THROW(authenticationService.authenticate());

    Poll::PollService pollService;
    pollService.send("Hi!!");
    ASSERT_EQ("Hi!!", pollService.message());

    IdentityAccess::SessionService sessionService;
    ASSERT_NO_THROW(sessionService.invalidate());

}
*/

TEST(ApplicationTest, TestWithMultiProcesses)
{

    // Create other processes.
    auto first = fork();
    auto second = fork();

    if ( first > 0 && second > 0 ) {

        printf("\nParent Process, my PID is %d \n", getpid());

        std::cout << "authenticating ... \n";
        IdentityAccess::AuthenticationService authenticationService;
        authenticationService.authenticate();
        std::cout << "... readResponse!" << std::endl;

        auto who = "I am the parent process.";
        Poll::PollService parentPollService;

        std::cout << "Saying ... " << who << std::endl;
        ASSERT_NO_THROW(parentPollService.send(who));

    } else if ( first == 0 && second > 0 ) {

        printf("\nFirst Child Process with PID %d  \n", getpid());

        auto who = "I am the first process.";
        Poll::PollService firstPollService;
        // ASSERT_EQ(something, firstPollService.message());
        std::cout << "Ok! We got it!! You said: " << firstPollService.message() << std::endl;

        std::cout << "Saying ... " << who << std::endl;
        ASSERT_NO_THROW(firstPollService.send(who));

    } else if ( first > 0 && second == 0 ) {

        printf("\nSecond Child Process, my PID is %d  \n", getpid());

        auto who = "I am the second process.";
        Poll::PollService secondPollService;
        // ASSERT_EQ(something, secondPollService.message());
        std::cout << "Ok! We got it!! You said: " << secondPollService.message() << std::endl;

        std::cout << "Saying ... " << who << std::endl;
        ASSERT_NO_THROW(secondPollService.send(who));

    } else {

        printf("\nThird Child Process, my PID is %d \n", getpid());

        auto who = "I am the third process.";
        Poll::PollService thirdPollService;
        // ASSERT_EQ(something, thirdPollService.message());
        std::cout << "Ok! We got it!! You said: " << thirdPollService.message() << std::endl;

        std::cout << "Saying ... " << who << std::endl;
        ASSERT_NO_THROW(thirdPollService.send(who));

    }

}
