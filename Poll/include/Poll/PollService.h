#ifndef Poll_PollService_INCLUDED
#define Poll_PollService_INCLUDED

#include <string>
#include "Foundation/SymbolVisibilityManager.h"

namespace Poll {


    class API PollService
    {
    public:
        PollService() = default;
        std::string message();
        void send(const std::string & message);

    };


}

#endif
