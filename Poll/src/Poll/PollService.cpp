#include <zconf.h>
#include "Poll/PollService.h"
#include "Foundation/Singleton.h"
#include "Adapter/Something/HiddenClass.h"

namespace Poll {


    std::string PollService::message()
    {
        // This class is not invokable from here (Internal Symbol)
        Something::HiddenClass hiddenClass;
        // hiddenClass.hiddenAction();

        // This must be invokable but only one instance (Protected Symbol)
        auto singleton = Foundation::Singleton::instance();
        return singleton->receive();
    }

    void PollService::send(const std::string & message)
    {
        // This class is not invokable from here (Internal Symbol)
        Something::HiddenClass hiddenClass;
        // hiddenClass.hiddenAction();

        // This must be invokable but only one instance (Protected Symbol)
        auto singleton = Foundation::Singleton::instance();
        singleton->send(message);
    }


}
