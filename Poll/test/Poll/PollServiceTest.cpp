#include <string>
#include <stdexcept>
#include "gtest/gtest.h"
#include "Poll/PollService.h"
#include "Foundation/Singleton.h"


TEST(PollServiceTest, MessageExchangeWithException)
{

    Poll::PollService pollService;
    ASSERT_THROW(pollService.message(), std::logic_error);

}

TEST(PollServiceTest, MessageExchangeWith)
{

    auto singleton = Foundation::Singleton::instance();
    singleton->createInternalObject();

    Poll::PollService pollService;
    pollService.send("The rabbit sit unafraid because it's smarter than its predator.");
    ASSERT_EQ(pollService.message(), "The rabbit sit unafraid because it's smarter than its predator.");

}
