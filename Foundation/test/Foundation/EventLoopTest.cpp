#include <string>
#include "gtest/gtest.h"
#include "Double/FakeConsumer.h"
#include "Foundation/EventLoop.h"
#include "Foundation/ActivityMediator.h"

TEST(EventLoopTest, CheckEventLoopRunning)
{

    auto notificationCounter = 0;
    ASSERT_NO_THROW(Foundation::ActivityMediator::allocateMemorySpace());
    ASSERT_TRUE(Foundation::EventLoop::instance().start());
    Foundation::EventLoop::instance().notifyTo(
        [&notificationCounter]() {
            notificationCounter++;
        }
    );

    sleep(1);
    ASSERT_TRUE(Foundation::ActivityMediator::sendRequest("Anything!!"));
    ASSERT_NO_THROW(std::thread anotherThread (
        [](){
            auto sleepDuration = 1;
            std::this_thread::sleep_for (std::chrono::seconds(sleepDuration));
            Foundation::EventLoop::instance().stop();
        });
        anotherThread.join();
    );
    ASSERT_GT(notificationCounter, 0);
    // ASSERT_NO_THROW(Foundation::ActivityMediator::deallocateMemorySpace());

}

TEST(BufferTest, SingleProcessSuccessfulBufferContentManipulation)
{

    auto notificationCounter = 0;
    ASSERT_NO_THROW(Foundation::ActivityMediator::allocateMemorySpace());
    ASSERT_TRUE(Foundation::EventLoop::instance().start());
    Foundation::EventLoop::instance().notifyTo(
        [&notificationCounter]() {
            notificationCounter++;
            Double::FakeConsumer::instance().handleRequest();
        }
    );

    sleep(1);
    ASSERT_TRUE(Foundation::ActivityMediator::sendRequest("Content into buffer"));
    ASSERT_NO_THROW(std::thread anotherThread (
        [=](){
            auto sleepDuration = 2;
            std::this_thread::sleep_for (std::chrono::seconds(sleepDuration));
            Foundation::EventLoop::instance().stop();
        });
        anotherThread.join();
    );

    std::string finalString;
    Foundation::ActivityMediator::readResponse(
        [&finalString](const std::string & response) {
            finalString = response;
        }
    );

    ASSERT_EQ(notificationCounter, 1);
    ASSERT_EQ(finalString, "Content into buffer was handled by the FakeConsumer!!");
    // ASSERT_NO_THROW(Foundation::ActivityMediator::deallocateMemorySpace());

}

TEST(EventLoopTest, UnsuccessfulSendRequestBecauseNoAllocatedSharedMemorySpace)
{

    // When there is no memory space available, the sendRequest MUST return false,
    // instead of crashing the application or throwing an exception.
    ASSERT_NO_THROW(Foundation::ActivityMediator::deallocateMemorySpace());
    ASSERT_FALSE(Foundation::ActivityMediator::sendRequest("Anything!!"));

}

TEST(EventLoopTest, UnsuccessfulSendRequestBecauseStoppedLoop) {

    ASSERT_NO_THROW(Foundation::ActivityMediator::allocateMemorySpace());
    ASSERT_FALSE(Foundation::ActivityMediator::sendRequest("Anything!!"));
    ASSERT_NO_THROW(Foundation::ActivityMediator::deallocateMemorySpace());

}

TEST(EventLoopTest, OtherThing)
{

    // ASSERT_NO_THROW(Foundation::ActivityMediator::allocateMemorySpace());
    // ASSERT_FALSE(Foundation::ActivityMediator::sendRequest("Anything!!"));

    for ( int i = 0; i < 9000000; ++i ) {

        std::string response;
        ASSERT_TRUE(Foundation::ActivityMediator::sendRequest("Request into buffer"));
        Foundation::ActivityMediator::readResponse(
            [&response](const std::string &_response) {
                response = _response;
            }
        );
        ASSERT_EQ(response, "Request into buffer with a response from the handler.");
        std::cout << "Response " << i << ".:" << response << "\n" << std::endl;

    }

}








TEST(BufferTest, DataBufferDataSharing)
{
    printf("\nParent Process, my PID is %d \n", getpid());

     // ASSERT_NO_THROW(Foundation::ActivityMediator::create());
     // ASSERT_NO_THROW(Foundation::ActivityMediator::sendRequest("I am the parent process!!"));

    // Create other processes.
    auto child = fork();
    if ( child == 0 ) {

        printf("\nChild Process, my PID is %d \n", getpid());
        // ASSERT_NO_THROW(Foundation::ActivityMediator::response([](const std::string &readResponse) {
        //        std::cout << "Response.: " << readResponse << std::endl;
        //    })
        // );

    }

}

TEST(BufferTest, InterprocessCommunication)
{

    // Create other processes.
    auto first  = fork();
    auto second = fork();

    if ( first > 0 && second > 0 ) {

        printf("\nParent Process, my PID is %d \n", getpid());

        ASSERT_NO_THROW(Foundation::ActivityMediator::allocateMemorySpace());
        ASSERT_NO_THROW(Foundation::ActivityMediator::sendRequest("I am the parent process"));

    } else if ( first == 0 && second > 0 ) {

        printf("\nFirst Child Process with PID %d  \n", getpid());

        // Double::FakeConsumer::instance().handleRequestUgly();
        ASSERT_NO_THROW(Foundation::ActivityMediator::readResponse([](const std::string &response) {
            std::cout << "Response.: " << response << std::endl;
        })
        );

        // ASSERT_NO_THROW(Foundation::ActivityMediator::sendRequest("I am the first process!!"));
        exit(EXIT_SUCCESS);

    } else if ( first > 0 && second == 0 ) {

        printf("\nSecond Child Process, my PID is %d  \n", getpid());

        // Double::FakeConsumer::instance().handleRequestUgly();
        // ASSERT_NO_THROW(Foundation::ActivityMediator::response([](const std::string & readResponse){
        //        std::cout << "Response.: " << readResponse << std::endl;
        //    })
        // );
        // ASSERT_NO_THROW(Foundation::ActivityMediator::sendRequest("I am the second process!!"));
        exit(EXIT_SUCCESS);

    } else {

        printf("\nThird Child Process, my PID is %d \n", getpid());

        // Double::FakeConsumer::instance().handleRequestUgly();
        // ASSERT_NO_THROW(Foundation::ActivityMediator::response([](const std::string & readResponse){
        //        std::cout << "Response.: " << readResponse << std::endl;
        //    })
        // );
        // ASSERT_NO_THROW(Foundation::ActivityMediator::sendRequest("I am the third process!!"));
        // exit(EXIT_SUCCESS);

    }

}
