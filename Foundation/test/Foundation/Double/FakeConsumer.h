#ifndef Double_FakeConsumer_INCLUDED
#define Double_FakeConsumer_INCLUDED

#include <string>
#include <chrono>
#include <thread>
#include "Foundation/ActivityMediator.h"

namespace Double {


    class FakeConsumer
    {
    public:
        // It deletes copy and move constructors and assign operators
        FakeConsumer(FakeConsumer &&) = delete;                   // Move construct
        FakeConsumer(FakeConsumer const &) = delete;              // Copy construct
        FakeConsumer & operator=(FakeConsumer &&) = delete;       // Move assign
        FakeConsumer & operator=(FakeConsumer const &) = delete;  // Copy assign

        ~FakeConsumer() = default;
        static FakeConsumer & instance();

        void handleRequest();
    private:
        FakeConsumer() = default;

        std::string receive();
        void send(const std::string &);

    };


    inline FakeConsumer & FakeConsumer::instance()
    {
        static FakeConsumer singleton;
        return singleton;
    }

    inline void FakeConsumer::send(const std::string &)
    {
        // It emulates the sendRequest latency time between the application and Registro.br servers.
        std::this_thread::sleep_for(std::chrono::seconds (1));
    }

    inline std::string FakeConsumer::receive()
    {
        return " was handled by the FakeConsumer!!";
    }

    inline void FakeConsumer::handleRequest()
    {
        auto handler = [&](const std::string & request) -> std::string {
            send(request);
            return request + receive();
        };

        Foundation::ActivityMediator::handleRequest(handler);
    }

}

#endif
