#include <iostream>
#include <stdexcept>
#include "boost/assert.hpp"
#include "Foundation/ActivityMediator.h"
#include "Foundation/ActivityTracer.h"
#include "Foundation/DebuggingMessage.h"
#include "Foundation/InterprocessString.h"
#include "Foundation/InterprocessStringVector.h"
#include "boost/interprocess/managed_shared_memory.hpp"

namespace Foundation {


    void ActivityMediator::allocateMemorySpace()
    {
        DataBuffer::create(1024);
    }

    void ActivityMediator::deallocateMemorySpace()
    {
        DataBuffer::destroy();
    }

    bool ActivityMediator::sendRequest(const std::string & content)
    {
        debuggingMessage("ActivityMediator::sendRequest('%s') from PID %d \n", content.c_str(), getpid());

        bool success = true;
        try {
            if (!DataBuffer::exists()) {
                // DataBuffer::sharedMemorySpaceAbsentNotification.disconnect_all_slots();
                // boost::bind(&mywin::paint, this,_1);
                return false;
            }

            auto readWriteMode = boost::interprocess::read_write;
            boost::interprocess::shared_memory_object sharedMemoryObject(
                    boost::interprocess::open_only,
                    MemoryIdentification::SHARED_MEMORY_OBJECT_NAME,
                    readWriteMode
                );

            sharedMemoryObject.truncate(sizeof(ActivityTracer));
            boost::interprocess::mapped_region mappedRegion(sharedMemoryObject, readWriteMode);
            auto mappedRegionAddress = mappedRegion.get_address();
            auto activityTracer = static_cast<ActivityTracer *>(mappedRegionAddress);

            /*
             * It makes this method fail in case the EventLoop is not listeningToBuffer and
             * it does it before mutexes and conditionals come into action.
             */
            if (!activityTracer->eventLoopOn) {
                activityTracer->eventLoopDownNotification();
                return false;
            }

            boost::interprocess::scoped_lock<boost::interprocess::interprocess_mutex> lock(activityTracer->mutex);
            if (activityTracer->contentIn)
                activityTracer->write.wait(lock);

            try {
                DataBuffer::insert(content);
                activityTracer->notifyBufferFull();

            } catch (std::exception & exception) {

                success = false;
                activityTracer->notifyBufferFull();
                debuggingMessage("(sendRequest-inner exception).: %s\n", exception.what());

            }

        } catch (std::exception & exception) {

            success = false;
            debuggingMessage("(sendRequest-outer exception).: %s\n", exception.what());

        }

        return success;
    }

    void ActivityMediator::handleRequest(const std::function<std::string(const std::string & response)> & handler)
    {
        try {
            auto readWriteMode = boost::interprocess::read_write;
            boost::interprocess::shared_memory_object sharedMemoryObject(
                    boost::interprocess::open_only,
                    MemoryIdentification::SHARED_MEMORY_OBJECT_NAME,
                    readWriteMode
                );
            sharedMemoryObject.truncate(sizeof(ActivityTracer));

            boost::interprocess::mapped_region mappedRegion(sharedMemoryObject, readWriteMode);
            auto mappedRegionAddress = mappedRegion.get_address();
            auto activityTracer = static_cast<ActivityTracer *>(mappedRegionAddress);

            boost::interprocess::scoped_lock<boost::interprocess::interprocess_mutex> lock(activityTracer->mutex);
            if (!activityTracer->isBufferReadyForHandling())
                activityTracer->handle.wait(lock);

            try {
                std::string request  = DataBuffer::allContent();
                std::string response = handler(request);
                if (!response.empty())
                    DataBuffer::insert(response);

                activityTracer->notifyBufferContentHandled();

            } catch (std::exception & exception) {

                activityTracer->notifyBufferContentHandled();
                std::cout << "(handleRequestUgly-inner) Ops.: " << exception.what() << std::endl;

            }

        } catch (std::exception & exception) {

            std::cout << "(handleRequestUgly-outer) Ops.: " << exception.what() << std::endl;

        }

    }

    void ActivityMediator::readResponse(const std::function<void(const std::string & response)> & reader)
    {
        debuggingMessage("ActivityMediator::readResponse(reader(...)) from PID %d \n", getpid());

        try {
            auto readWriteMode = boost::interprocess::read_write;
            boost::interprocess::shared_memory_object sharedMemoryObject(
                    boost::interprocess::open_only,
                    MemoryIdentification::SHARED_MEMORY_OBJECT_NAME,
                    readWriteMode
                );
            sharedMemoryObject.truncate(sizeof(ActivityTracer));

            boost::interprocess::mapped_region mappedRegion(sharedMemoryObject, readWriteMode);
            auto mappedRegionAddress = mappedRegion.get_address();
            auto activityTracer = static_cast<ActivityTracer *>(mappedRegionAddress);

            boost::interprocess::scoped_lock<boost::interprocess::interprocess_mutex> lock(activityTracer->mutex);
            if (!activityTracer->isBufferReadyForReading())
                activityTracer->read.wait(lock);

            try {
                reader(DataBuffer::allContent());
                activityTracer->notifyBufferEmpty();

            } catch (std::exception & exception) {

                activityTracer->notifyBufferEmpty();
                std::cout << "(readResponse-inner) Ops.: " << exception.what() << std::endl;

            }

        } catch (std::exception & exception) {
            std::cout << "(readResponse-outer) Ops.: " << exception.what() << std::endl;
        }
    }

    void ActivityMediator
        ::DataBuffer::destroy()
        {
            debuggingMessage("ActivityMediator::destroy() from PID %d \n", getpid());

            boost::interprocess::shared_memory_object::remove(MemoryIdentification::SHARED_MEMORY_SEGMENT_NAME);
            try {
                boost::interprocess::managed_shared_memory sharedSegment(
                        boost::interprocess::open_only,
                        MemoryIdentification::SHARED_MEMORY_SEGMENT_NAME
                    );

            } catch (boost::interprocess::interprocess_exception & exception) {
                BOOST_ASSERT_MSG(
                        exception.get_error_code() == boost::interprocess::not_found_error,
                        "Shared memory should not exist."
                    );
            }
        }


    bool ActivityMediator
        ::DataBuffer::exists()
        {
            auto found = false;
            try {
                boost::interprocess::managed_shared_memory sharedSegment(
                        boost::interprocess::open_only,
                        MemoryIdentification::SHARED_MEMORY_SEGMENT_NAME
                    );

                found = sharedSegment.check_sanity();

            } catch (boost::interprocess::interprocess_exception & exception) {
                BOOST_ASSERT(exception.get_error_code() == boost::interprocess::not_found_error);
            }

            return found;
        }

    std::string ActivityMediator
        ::DataBuffer::allContent()
        {
            boost::interprocess::managed_shared_memory sharedSegment(
                    boost::interprocess::open_only,
                    MemoryIdentification::SHARED_MEMORY_SEGMENT_NAME
                );

            auto typeSizePair = sharedSegment.find<InterprocessStringVector>(MemoryIdentification::SHARED_DATA_STRUCTURE_NAME);
            BOOST_ASSERT_MSG(typeSizePair.first->size() == 1, "Number of vector items before reading MUST be one.");

            std::string content;
            auto vector = typeSizePair.first;
            if ( vector->empty() )
                return content;

            auto interprocessContent = vector->front();
            content.assign(interprocessContent.begin(), interprocessContent.end());

            vector->clear();
            BOOST_ASSERT_MSG(vector->empty(), "Number of vector items after reading MUST be zero.");

            return content;
        }

    void ActivityMediator
        ::DataBuffer::create(unsigned int memorySize)
        {
            debuggingMessage("ActivityMediator::create() from PID %d \n", getpid());

            destroy();
            try {
                ActivityTracer::create();

                boost::interprocess::managed_shared_memory sharedSegment(
                        boost::interprocess::create_only,
                        MemoryIdentification::SHARED_MEMORY_SEGMENT_NAME,
                        memorySize                                          // Shared memory object size in bytes
                    );

                BOOST_ASSERT(sharedSegment.check_sanity());
                BOOST_ASSERT_MSG(sharedSegment.get_num_named_objects() == 0, "No object should exist into the shared memory.");

                /*
                 * This vector is fully constructed in shared memory.
                 * All pointers buffers are constructed in the same shared memory segment.
                 * This vector can be safely accessed from other processes.
                 */
                StringAllocator stringAllocator(sharedSegment.get_segment_manager());
                sharedSegment.construct<InterprocessStringVector>(MemoryIdentification::SHARED_DATA_STRUCTURE_NAME)(stringAllocator);

                BOOST_ASSERT_MSG(sharedSegment.get_num_named_objects() == 1, "Only one object should exist into the shared memory.");

            } catch (std::exception & exception) {
                std::cout << "(create) Ops.: " << exception.what() << std::endl;
            }
        }

    void ActivityMediator
        ::DataBuffer::insert(const std::string & content)
        {
            boost::interprocess::managed_shared_memory sharedSegment(
                    boost::interprocess::open_only,
                    MemoryIdentification::SHARED_MEMORY_SEGMENT_NAME
                );

            CharacterAllocator charAllocator(sharedSegment.get_segment_manager());
            InterprocessString interprocessContent(content.c_str(), charAllocator);

            auto typeSizePair = sharedSegment.find<InterprocessStringVector>(MemoryIdentification::SHARED_DATA_STRUCTURE_NAME);
            auto vector       = typeSizePair.first;
            vector->insert(vector->begin(), interprocessContent);
        }


}
