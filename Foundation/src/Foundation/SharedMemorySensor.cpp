#include <unistd.h>
#include "Foundation/SharedMemorySensor.h"
#include "Foundation/DebuggingMessage.h"

namespace Foundation {


    void SharedMemorySensor::onSharedMemoryAbsent()
    {
        debuggingMessage("SharedMemorySensor::onSharedMemoryAbsent() from PID %d \n", getpid());
    }


}
