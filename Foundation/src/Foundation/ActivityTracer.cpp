#include "Foundation/ActivityTracer.h"
#include "Foundation/EventLoopSensor.h"
#include "Foundation/DebuggingMessage.h"
#include "Foundation/MemoryIdentification.h"
#include "boost/interprocess/mapped_region.hpp"
#include "boost/interprocess/shared_memory_object.hpp"

namespace Foundation {


    ActivityTracer::ActivityTracer()
        : contentIn(false),
          contentHandled(false),
          eventLoopOn(false)
    {
        debuggingMessage("ActivityTracer::ActivityTracer() from PID %d \n", getpid());

        eventLoopDownNotification.connect(boost::bind(&EventLoopSensor::onEventLoopDown));
        eventLoopTurnOnNotification.connect(boost::bind(&EventLoopSensor::onEventLoopTurnedOn));
    }

    ActivityTracer * ActivityTracer::instance()
    {
        auto readWriteMode = boost::interprocess::read_write;
        boost::interprocess::shared_memory_object sharedMemoryObject(
                boost::interprocess::open_only,
                MemoryIdentification::SHARED_MEMORY_OBJECT_NAME,
                readWriteMode
            );

        sharedMemoryObject.truncate(sizeof(ActivityTracer));
        boost::interprocess::mapped_region mappedRegion(sharedMemoryObject, readWriteMode);
        auto mappedRegionAddress = mappedRegion.get_address();
        auto activityTracer = static_cast<ActivityTracer *>(mappedRegionAddress);

        return activityTracer;
    }

    void ActivityTracer::perform(const std::function<void (ActivityTracer *)> & action)
    {
        auto readWriteMode = boost::interprocess::read_write;
        boost::interprocess::shared_memory_object sharedMemoryObject(
                boost::interprocess::open_only,
                MemoryIdentification::SHARED_MEMORY_OBJECT_NAME,
                readWriteMode
            );

        sharedMemoryObject.truncate(sizeof(ActivityTracer));
        boost::interprocess::mapped_region mappedRegion(sharedMemoryObject, readWriteMode);
        auto mappedRegionAddress = mappedRegion.get_address();
        auto activityTracer = static_cast<ActivityTracer *>(mappedRegionAddress);

        action(activityTracer);
    }

    bool ActivityTracer::isEventLoopOn()
    {
        return eventLoopOn;
    }

    void ActivityTracer::turnOnEventLoop()
    {
        if (eventLoopDownNotification.empty())
            eventLoopDownNotification.connect(boost::bind(&EventLoopSensor::onEventLoopDown));

        if (eventLoopTurnOnNotification.empty())
            eventLoopTurnOnNotification.connect(boost::bind(&EventLoopSensor::onEventLoopTurnedOn));

        eventLoopOn = true;
        eventLoopTurnOnNotification();
    }

    void ActivityTracer::turnOffEventLoop()
    {
        eventLoopOn = false;
        eventLoopDownNotification.disconnect_all_slots();
        eventLoopTurnOnNotification.disconnect_all_slots();
    }

    bool ActivityTracer::hasBufferUnhandledContent()
    {
        return contentIn && !contentHandled;
    }

    bool ActivityTracer::isBufferReadyForHandling()
    {
        bool bufferNotReadyForConsuming = !contentIn || contentHandled;
        return !bufferNotReadyForConsuming;
    }

    bool ActivityTracer::isBufferReadyForReading()
    {
        bool bufferNotReadyForReading = !contentIn || !contentHandled;
        return !bufferNotReadyForReading;
    }

    void ActivityTracer::notifyBufferEmpty()
    {
        write.notify_one();
        contentIn      = false;
        contentHandled = false;
    }

    void ActivityTracer::notifyBufferFull()
    {
        handle.notify_one();
        contentIn      = true;
        contentHandled = false;
    }

    void ActivityTracer::notifyBufferContentHandled()
    {
        read.notify_one();
        contentIn      = true;
        contentHandled = true;
    }

    void ActivityTracer::create()
    {
        debuggingMessage("ActivityTracer::create() from PID %d \n", getpid());

        destroy();
        auto readWriteMode = boost::interprocess::read_write;
        boost::interprocess::shared_memory_object sharedMemoryObject(
                boost::interprocess::create_only,
                MemoryIdentification::SHARED_MEMORY_OBJECT_NAME,
                readWriteMode
            );
        sharedMemoryObject.truncate(sizeof(ActivityTracer));

        boost::interprocess::mapped_region mappedRegion(sharedMemoryObject, readWriteMode);
        new(mappedRegion.get_address()) ActivityTracer;
    }

    void ActivityTracer::destroy()
    {
        boost::interprocess::shared_memory_object::remove(MemoryIdentification::SHARED_MEMORY_OBJECT_NAME);
        try {
            boost::interprocess::shared_memory_object::remove(MemoryIdentification::SHARED_MEMORY_OBJECT_NAME);
        } catch (boost::interprocess::interprocess_exception & exception) {
            BOOST_ASSERT_MSG(exception.get_error_code() == boost::interprocess::not_found_error, "Shared memory should not exist.");
        }
    }


}
