#include <iostream>
#include <stdexcept>
#include "Foundation/ActivityTracer.h"
#include "Foundation/EventLoop.h"
#include "Foundation/DebuggingMessage.h"
#include "Foundation/MemoryIdentification.h"
#include "boost/interprocess/mapped_region.hpp"
#include "boost/interprocess/shared_memory_object.hpp"

namespace Foundation {


    EventLoop & EventLoop::instance()
    {
        debuggingMessage("\nEventLoop::instance() from PID %d \n", getpid());
        static EventLoop singleton;
        return singleton;
    }

    EventLoop::EventLoop()
        : listeningToBuffer(false)
    {
        debuggingMessage("EventLoop::EventLoop() from PID %d \n", getpid());
    }

    EventLoop::~EventLoop()
    {
        try {
            if (listeningToBuffer)
                stop();
        } catch (...) {}
    }

    void EventLoop::stop()
    {
        debuggingMessage("EventLoop::stop() from PID %d \n", getpid());

        try {
            auto readWriteMode = boost::interprocess::read_write;
            boost::interprocess::shared_memory_object sharedMemoryObject(
                    boost::interprocess::open_only,
                    MemoryIdentification::SHARED_MEMORY_OBJECT_NAME,
                    readWriteMode
                );
            sharedMemoryObject.truncate(sizeof(ActivityTracer));

            boost::interprocess::mapped_region mappedRegion(sharedMemoryObject, readWriteMode);
            auto mappedRegionAddress = mappedRegion.get_address();
            auto bufferSharedTracer  = static_cast<ActivityTracer *>(mappedRegionAddress);

            bufferSharedTracer->turnOffEventLoop();
            backgroundRunner.join();

            /*
             * If, and only if, the ActivityTracer is EventLoop disabled, and the thread loop is stopped,
             * the listeningToBuffer variable is marked false.
             */
            listeningToBuffer = bufferSharedTracer->isEventLoopOn();

            ///
            contentToHandleNotification.disconnect_all_slots();

            debuggingMessage("\nBackground Thread Joined");

        } catch (std::exception & exception) {
            std::cout << "Exception stopping the EventLoop " << exception.what() << std::endl;
            throw;
        }
    }

    bool EventLoop::start()
    {
        debuggingMessage("EventLoop::start() from PID %d \n", getpid());

        bool started     = true;
        backgroundRunner = std::thread([&](){

            try {
                auto readWriteMode = boost::interprocess::read_write;
                boost::interprocess::shared_memory_object sharedMemoryObject(
                        boost::interprocess::open_only,
                        MemoryIdentification::SHARED_MEMORY_OBJECT_NAME,
                        readWriteMode
                    );

                sharedMemoryObject.truncate(sizeof(ActivityTracer));
                boost::interprocess::mapped_region mappedRegion(sharedMemoryObject, readWriteMode);
                auto mappedRegionAddress = mappedRegion.get_address();
                auto activityTracer  = static_cast<ActivityTracer *>(mappedRegionAddress);

                activityTracer->turnOnEventLoop();
                do {
                    activityTracer = static_cast<ActivityTracer *>(mappedRegionAddress);
                    if ( activityTracer->hasBufferUnhandledContent() )
                        contentToHandleNotification();

                } while (activityTracer->isEventLoopOn());

            } catch (boost::interprocess::interprocess_exception & exception) {

                std::cout << "Exception into the event loop " << exception.what() << std::endl;
                BOOST_ASSERT_MSG(
                        exception.get_error_code() == boost::interprocess::not_found_error,
                        "Shared memory should not exist."
                );

            } catch (std::exception & exception) {
                started = false;
                std::cout << "Exception into the event loop " << exception.what() << std::endl;
            }

        });

        listeningToBuffer = started;
        return started;
    }

    void EventLoop::notifyTo(const std::function<void ()> & notificationHandler)
    {
        debuggingMessage("EventLoop::notifyTo(...) from PID %d \n", getpid());
        contentToHandleNotification.connect([=](){
            notificationHandler();
        });
    }


}
