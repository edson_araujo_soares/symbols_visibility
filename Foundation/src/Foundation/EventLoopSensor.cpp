#include <future>
#include <thread>
#include <iostream>
#include <unistd.h>
#include "boost/process.hpp"
#include "boost/filesystem.hpp"
#include "boost/asio/io_service.hpp"
#include "Foundation/EventLoopSensor.h"
#include "Foundation/ActivityMediator.h"
#include "Foundation/DebuggingMessage.h"

namespace Foundation {


    void EventLoopSensor::onEventLoopDown()
    {
        debuggingMessage("EventLoopSensor::onEventLoopDown() from PID %d \n", getpid());
        // ActivityMediator::allocateMemorySpace();

        /*
        constexpr auto PROCESS_BINARY_FILENAME = "Consumer";
        auto path = boost::filesystem::current_path();
        if (!boost::filesystem::is_directory(path))
            exit(1);

        path.append(PROCESS_BINARY_FILENAME);
        if (!boost::filesystem::is_regular_file(path))
            exit(1);

        debuggingMessage("Creating another process from binary file: %s \n", path.c_str());

         *
         * Passing an instance of boost::asio::io_service to the launching function automatically
         * cause it to wait asynchronously for the exit, so no call of boost::process::wait is needed.
         *
        boost::asio::io_service ioService;

        std::future<std::string> err_data;
        std::future<std::string> output_data;

        boost::process::child childProcess(path, //set the input
            boost::process::std_in.close(),
            boost::process::std_out > output_data,
            boost::process::std_err > err_data,
            ioService
        );

        ioService.run();
        ioService.stop();
        childProcess.terminate();

        auto err    =  err_data.get();
        auto output =  output_data.get();

        if (!err.empty())
            std::cout << "Another Process Err.: " << err << std::endl;

        if (!output.empty())
            std::cout << "Another Process Output.: " << output << std::endl;
        */

    }

    void EventLoopSensor::onEventLoopTurnedOn()
    {
        debuggingMessage("EventLoopSensor::onEventLoopTurnedOn() from PID %d \n", getpid());
    }


}
