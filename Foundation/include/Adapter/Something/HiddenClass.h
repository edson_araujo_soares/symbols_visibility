#ifndef Something_HiddenClass_INCLUDED
#define Something_HiddenClass_INCLUDED

#include "Foundation/SymbolVisibilityManager.h"

namespace Something {


    class LOCAL HiddenClass
    {
    public:
        HiddenClass() = default;
        void hiddenAction();

    };


}

#endif
