#ifndef Foundation_InterprocessStringVector_INCLUDED
#define Foundation_InterprocessStringVector_INCLUDED

#include "Foundation/InterprocessString.h"
#include "boost/interprocess/containers/vector.hpp"
#include "boost/interprocess/allocators/allocator.hpp"
#include "boost/interprocess/managed_shared_memory.hpp"

namespace Foundation {


    typedef boost::interprocess::allocator<InterprocessString, boost::interprocess::managed_shared_memory::segment_manager> StringAllocator;
    typedef boost::interprocess::vector<InterprocessString, StringAllocator> InterprocessStringVector;


}

#endif
