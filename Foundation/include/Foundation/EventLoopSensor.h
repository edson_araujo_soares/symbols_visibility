#ifndef Foundation_EventLoopSensor_INCLUDED
#define Foundation_EventLoopSensor_INCLUDED

#include "Foundation/SymbolVisibilityManager.h"

namespace Foundation {


    class API EventLoopSensor
    {
    public:
        static void onEventLoopDown();
        static void onEventLoopTurnedOn();

    private:
        EventLoopSensor() = default;

    };


}

#endif
