#ifndef Foundation_DataBufferEventLoop_INCLUDED
#define Foundation_DataBufferEventLoop_INCLUDED

#include <string>
#include <thread>
#include <functional>
#include "boost/signals2.hpp"
#include "Foundation/SymbolVisibilityManager.h"


/*
 *
 * TODO
 *
 * - Add counter to the event loop
 * - Add life timer to the event loop
 * - Improve the robustness of the component in general. No seg-fault should happen.
 *
 */

namespace Foundation {


    class API EventLoop
    {
    public:
        // It deletes copy and move constructors and assign operators
        EventLoop(EventLoop &&) = delete;                   // Move construct
        EventLoop(EventLoop const &) = delete;              // Copy construct
        EventLoop & operator=(EventLoop &&) = delete;       // Move assign
        EventLoop & operator=(EventLoop const &) = delete;  // Copy assign

        ~EventLoop();
        static EventLoop & instance();

        void stop();
        bool start();
        void notifyTo(const std::function<void()> & notificationHandler);

    private:
        EventLoop();

        bool listeningToBuffer;
        std::thread backgroundRunner;
        boost::signals2::signal<void()> contentToHandleNotification;

    };


}

#endif
