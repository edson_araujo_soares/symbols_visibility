#ifndef Foundation_MemoryIdentification_INCLUDED
#define Foundation_MemoryIdentification_INCLUDED

#include "Foundation/SymbolVisibilityManager.h"

namespace Foundation {


    struct API MemoryIdentification
    {
        // It deletes copy and move constructors and assign operators
        MemoryIdentification()  = delete;
        ~MemoryIdentification() = delete;
        MemoryIdentification(MemoryIdentification &&) = delete;                   // Move construct
        MemoryIdentification(MemoryIdentification const &) = delete;              // Copy construct
        MemoryIdentification & operator=(MemoryIdentification &&) = delete;       // Move assign
        MemoryIdentification & operator=(MemoryIdentification const &) = delete;  // Copy assign

        static constexpr const char * SHARED_DATA_STRUCTURE_NAME = "__VECTOR_STRUCTURE__";
        static constexpr const char * SHARED_MEMORY_SEGMENT_NAME = "__BUFFER_STRUCTURE__";
        static constexpr const char * SHARED_MEMORY_OBJECT_NAME  = "__OBJECT_SPACE__";

    };


}

#endif
