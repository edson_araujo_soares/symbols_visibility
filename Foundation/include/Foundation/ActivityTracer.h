#ifndef Foundation_ActivityTracer_INCLUDED
#define Foundation_ActivityTracer_INCLUDED

#include <functional>
#include "boost/signals2.hpp"
#include "Foundation/SymbolVisibilityManager.h"
#include "boost/interprocess/sync/interprocess_mutex.hpp"
#include "boost/interprocess/sync/interprocess_condition.hpp"

namespace Foundation {


    class API ActivityTracer
    {
    public:
        // It deletes copy and move constructors and assign operators
        ActivityTracer(ActivityTracer &&) = delete;                   // Move construct
        ActivityTracer(ActivityTracer const &) = delete;              // Copy construct
        ActivityTracer & operator=(ActivityTracer &&) = delete;       // Move assign
        ActivityTracer & operator=(ActivityTracer const &) = delete;  // Copy assign

        ~ActivityTracer() = default;

        static ActivityTracer * instance();
        static void perform(const std::function<void(ActivityTracer *)> & action);

        bool isEventLoopOn();
        void turnOnEventLoop();
        void turnOffEventLoop();

        /*
         * State machine like notifications that represent the possible states of the data buffer.
         * This state machine has three states:
         *      1. ActivityMediator Empty          : It says the buffer can be filled with fresh content.
         *      2. ActivityMediator Full           : It says the buffer has content to be handled.
         *      3. ActivityMediator Content Handled: It says the buffer has content ready to be read.
         *
         * Once the buffer content has been readResponse it returns to the first state, buffer empty.
         * A short, general scheme of the states and the related actions would be:
         *      BufferEmpty          ―> (action required) Write fresh content to the data buffer.
         *      BufferFull           ―> (action required) Handle the content into the buffer.
         *      BufferContentHandled ―> (action required) Read the buffer content, therefore emptying it.
         */
        void notifyBufferEmpty();
        void notifyBufferFull();
        void notifyBufferContentHandled();

        /*
         * It allows one to say when there is content to handle into the data buffer.
         * If the result of this function is true, then the handler can be called.
         */
        bool hasBufferUnhandledContent();

        /*
         * Once the handler has been called it must be guaranteed that no other operation
         * is on process before starting to handle a new one. If this function returns true,
         * a new operation can be started with safety.
         */
        bool isBufferReadyForHandling();

        /*
         * Once the handler is called and the operation is handled this function will return true warning
         * that the content into the buffer is ready for being read.
         */
        bool isBufferReadyForReading();

    private:
        ActivityTracer();

        static void create();
            /// It creates this class object into the shared memory space.

        static void destroy();
            /// It removes this class object from the shared memory space.

        boost::signals2::signal<void()> eventLoopDownNotification;
        boost::signals2::signal<void()> eventLoopTurnOnNotification;

        // It tells if there is any content into the buffer.
        bool contentIn;

        // It tells if the content into the buffer was handled already.
        bool contentHandled;

        /*
         * It defines the EventLoop component status.
         *
         * If the value is false, then the EventLoop will stop.
         * If it is true, then the EventLoop will keep listeningToBuffer from
         * the shared memory space until the value be changed.
         */
        bool eventLoopOn;

        // Mutex to protect access to the buffer.
        boost::interprocess::interprocess_mutex mutex;

        // Condition to wait when the buffer is readResponse.
        boost::interprocess::interprocess_condition  read;

        // Condition to wait when the buffer is full.
        boost::interprocess::interprocess_condition  write;

        // Condition to wait when the buffer is full and not handled.
        boost::interprocess::interprocess_condition  handle;


        friend class ActivityMediator;
    };


}

#endif
