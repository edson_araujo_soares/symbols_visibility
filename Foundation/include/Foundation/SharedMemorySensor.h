#ifndef Foundation_SharedMemorySensor_INCLUDED
#define Foundation_SharedMemorySensor_INCLUDED

#include "Foundation/SymbolVisibilityManager.h"

namespace Foundation {


    class API SharedMemorySensor
    {
    public:
        static void onSharedMemoryAbsent();

    private:
        SharedMemorySensor() = default;

    };


}

#endif
