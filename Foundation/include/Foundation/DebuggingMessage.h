#ifndef Foundation_DebuggingMessage_INCLUDED
#define Foundation_DebuggingMessage_INCLUDED

#include <cstdio>

namespace Foundation {


#ifdef NDEBUG
    #define debuggingMessage
#else
    #define debuggingMessage printf
#endif


}

#endif
