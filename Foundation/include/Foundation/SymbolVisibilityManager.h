#ifndef Foundation_SymbolVisibilityManager_INCLUDED
#define Foundation_SymbolVisibilityManager_INCLUDED

#if !defined(API)
    #if defined (__GNUC__) && (__GNUC__ >= 4)
        #define API __attribute__ ((visibility ("default")))
    #else
        #define API
    #endif
#endif

#if !defined(API_UNIQUE)
    #if defined (__GNUC__) && (__GNUC__ >= 4)
        #define API_UNIQUE __attribute__ ((visibility ("protected")))
    #else
        #define API_UNIQUE
    #endif
#endif

#if !defined(LOCAL)
    #if defined (__GNUC__) && (__GNUC__ >= 4)
        #define LOCAL __attribute__ ((visibility ("internal")))
    #else
        #define LOCAL
    #endif
#endif


#endif
