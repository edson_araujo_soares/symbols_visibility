#ifndef Foundation_ActivityMediator_INCLUDED
#define Foundation_ActivityMediator_INCLUDED

#include <string>
#include <functional>
#include "boost/signals2.hpp"
#include "Foundation/MemoryIdentification.h"
#include "Foundation/SymbolVisibilityManager.h"

namespace Foundation {


    /*
     * This is the class used by the Producer and the Consumer for exchanging,
     * and manipulating, content into a data buffer between one or more processes.
     */
    class API ActivityMediator
    {
    public:
        ActivityMediator() = delete;
        ~ActivityMediator() = delete;

        /*
         * Creates and destroy an interprocess shared memory space for the operations.
         */
        static void allocateMemorySpace();
        static void deallocateMemorySpace();

        /*
         * It places content into the data buffer.
         *
         * @param content The content to be placed into the data buffer.
         */
        static bool sendRequest(const std::string & content);

        /*
         * It allows edition of the content into the data buffer.
         * If the content into the data buffer needs more than just to be read, this is the function to use.
         *
         * Handling is the process of, first, reading from the data buffer, then writing new content back
         * into the data buffer, two actions, in the same operation. Handling is the manipulation, the edition,
         * of the content currently into the buffer by consuming from the data buffer, then producing
         * new content as a result of the handling, back into data buffer.
         *
         * Note.: This is an alternative to the two channels producer consumer.
         *
         * @param request The content of the last request sent into the data buffer.
         * @return The new content, result of the manipulation, to be set into the data buffer.
         */
        static void handleRequest(const std::function<std::string(const std::string & content)> & handler);

        /*
         * It just reads the content currently into the buffer.
         *
         * @param response The content into the buffer.
         */
        static void readResponse(const std::function<void(const std::string & content)> & reader);

    private:
        class DataBuffer {
        public:
            DataBuffer() = delete;
            ~DataBuffer() = delete;
            static bool exists();
            static void destroy();
            static std::string allContent();
            static void create(unsigned int memorySize);
            static void insert(const std::string & content);

            boost::signals2::signal<void()> sharedMemorySpaceAbsentNotification;

        };

    };


}

#endif
