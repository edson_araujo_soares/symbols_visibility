#ifndef Consumer_Counter_INCLUDED
#define Consumer_Counter_INCLUDED

#include "Foundation/SymbolVisibilityManager.h"

namespace Consumer {


    class API Counter
    {
    public:
        Counter() = default;
        void count();

    };


}

#endif
