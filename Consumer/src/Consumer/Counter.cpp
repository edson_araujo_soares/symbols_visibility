#include <iostream>
#include "Consumer/Counter.h"

namespace Consumer {


    void Counter::count()
    {
        auto bound = 9;
        for ( int i = 0; i <= bound; ++i ) {

            if ( i == 0 )
                std::cout << "counting ... ";

            if ( i != bound )
                std::cout << i << ", ";
            else
                std::cout << i << "." << std::endl;

        }

    }


}
