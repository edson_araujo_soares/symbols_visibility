#include <thread>
#include <iostream>
#include "Foundation/EventLoop.h"
#include "Foundation/ActivityMediator.h"

int main() {

    // Foundation::ActivityMediator::allocateMemorySpace();

    auto notificationCounter = 0;
    Foundation::EventLoop::instance().start();
    Foundation::EventLoop::instance().notifyTo(
    [&](){
                        notificationCounter++;
                        Foundation::ActivityMediator::handleRequest(
                        [&](const std::string & request) -> std::string {
                                    return request + " with a response from the handler.";
                                }
                            );
                });

    std::thread anotherThread (
        [](){
            std::this_thread::sleep_for (std::chrono::seconds(900));
            Foundation::EventLoop::instance().stop();
        });
    anotherThread.join();

    std::cout << std::endl;
    std::cout << "Notification Counter.: " << notificationCounter << std::endl;

    return 0;

}
